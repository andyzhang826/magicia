#pragma once

#include "Global.h"

#include <Windows.h>

namespace Magicia
{
	LRESULT __stdcall WndProcMagiciaMain(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
}