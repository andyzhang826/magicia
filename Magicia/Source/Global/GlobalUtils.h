#pragma once

#include <Windows.h>
#include <fstream>
#include <string>
#include <map>


// Comment out this to avoid logging.
#define __LOGENABLED__

namespace Magicia
{
	class ULog
	{
	public:
		// Log to LogMagicia.log by default.
		static void LogLastErrorToFile();
	};

	extern ULog Log;

	class UWindowsManager
	{
	public:
		void Init(HINSTANCE InProgram);
		void RegisterWindowClass(std::wstring InWindowClassName, WNDPROC InWindowProcedure);
		void InitializeWindow(std::wstring InWindowClassName, std::wstring InWindowName);
		// @return: Exit code of the program.
		int MessageLoop();
	private:
		// Store the handle of the program itself.
		HINSTANCE Program;
		// Maps windows class name to its corresponding window procedure.
		std::map<std::wstring, WNDPROC> WindowsProcedures;
		// Maps windows class name to its corresponding window class.
		std::map<std::wstring, ATOM> WindowsClasses;
		// Maps windows name to its corresponding window handle.
		std::map<std::wstring, HWND> WindowsHandles;
	};

	extern UWindowsManager WindowsManager;
}

