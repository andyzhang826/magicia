#include "GlobalUtils.h"

namespace Magicia
{
	void ULog::LogLastErrorToFile()
	{
		// Get error message.
		wchar_t ErrorMessage[256];
		FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			ErrorMessage, sizeof(ErrorMessage), NULL);

		// Log error message to file.
		std::wofstream LogFileOutputer("LogMagicia.log", std::ios_base::app);
		LogFileOutputer << ErrorMessage << std::endl;
	}

	ULog Log;

	void UWindowsManager::Init(HINSTANCE InProgram)
	{
		Program = InProgram;
	}

	void UWindowsManager::RegisterWindowClass(std::wstring InWindowClassName, WNDPROC InWindowProcedure)
	{
		WNDCLASSEXW wcex = { 0 };

		wcex.cbSize = sizeof(WNDCLASSEXW);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		// @TODO: make sure corresponding window procedure exists.
		wcex.lpfnWndProc = InWindowProcedure;
		wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor = LoadCursor(NULL, IDC_CROSS);
		wcex.hInstance = Program;
		wcex.hbrBackground = static_cast<HBRUSH>(GetStockObject(LTGRAY_BRUSH));
		wcex.lpszClassName = InWindowClassName.c_str();
		wcex.lpszMenuName = NULL;
		
		WindowsClasses[InWindowClassName] = RegisterClassExW(&wcex);
		WindowsProcedures[InWindowClassName] = InWindowProcedure;
	}

	void UWindowsManager::InitializeWindow(std::wstring InWindowClassName, std::wstring InWindowName)
	{
		WindowsHandles[InWindowName] = CreateWindowExW(0L, InWindowClassName.c_str(), InWindowName.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, Program, nullptr);
		ShowWindow(WindowsHandles[InWindowName], SW_SHOWMAXIMIZED);
		UpdateWindow(WindowsHandles[InWindowName]);
	}

	int UWindowsManager::MessageLoop()
	{
		MSG msg;

		while (GetMessageW(&msg, nullptr, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}

		return static_cast<int>(msg.wParam);
	}

	UWindowsManager WindowsManager;
}

