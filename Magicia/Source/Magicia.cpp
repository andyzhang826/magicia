#include "Magicia.h"

namespace Magicia
{
	// @return: Exit code of the program.
	int Main(HINSTANCE hInstance)
	{
		WindowsManager.Init(hInstance);

		WindowsManager.RegisterWindowClass(L"MagiciaMain", WndProcMagiciaMain);

		WindowsManager.InitializeWindow(L"MagiciaMain", L"MagiciaMain");

		return WindowsManager.MessageLoop();
	}
}

// NOTE: wWinMain can not reside in a namespace!
int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	return Magicia::Main(hInstance);
}






